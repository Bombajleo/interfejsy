﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rekursja
{
    class TestRekursjon
    {
        public float Test(int i)
        {

            if (i == 1)
                return 1;
            if (i == 2)
                return 1;

            return Test(i - 1) + Test(i - 2);
         
               
            
        }
    }
}
