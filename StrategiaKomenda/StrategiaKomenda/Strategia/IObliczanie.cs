﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategiaKomenda.Strategia
{
    public interface IObliczanie
    {
         float Oblicz(float a, float b);
    }

}
