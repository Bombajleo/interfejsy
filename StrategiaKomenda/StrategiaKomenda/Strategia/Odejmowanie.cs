﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategiaKomenda.Strategia
{
    class Odejmowanie : IObliczanie
    {
        public float Oblicz(float a, float b)
        {
            return a - b;
        }
    }
}
