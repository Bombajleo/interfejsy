﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfejsy
{
    class Buldog : IDog
    {
        public void podaj()
        {
            Console.WriteLine("Mati podaje grzecznie dwie nogi");
        }

        public void przynies()
        {
            Console.WriteLine("Podaje Ci ringo");
        }

        public void zjedz()
        {
            throw new NotImplementedException();
        }
    }
}
