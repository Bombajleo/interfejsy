﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanko28._12
{
    class Prostokąt : IShape
    {
        private float Area;
        private float Circum;
        private List<int> sides;
        
        public Prostokąt()
        {
            RandomizeShape();
        }
        public float GetArea()
        {
            return Area;
        }

        public float GetCicrum()
        {
            return Circum;
        }

        public IShape RandomizeShape()
      {
         sides = new List<int>();
        Random rand = new Random();

        do
           {
              for (int i = 0; i < 4; i++)
              {
              sides.Add(rand.Next(1, 100));
                }
            } while (Waliduj());
           return this;
        }

        public IShape RandomizeShape(Random rand)
        {
            sides = new List<int>();
            int a = rand.Next(1, 100);
            int b = rand.Next(1, 100);

            do
            {
                for (int i = 0; i < 2; i++)
                {
                    sides.Add(a);
                    sides.Add(b);

                }
            } while (Waliduj());
            return this;
        }

        public float SetArea()
        {
            sides.Sort();

            int a = sides[0];
            int b = sides[1];
           

            Area = (float)Math.Sqrt((a * b));
            return Area;
           
        }

        public float SetCircum()
        {
            return Circum = sides.Sum();
        }

        public bool Waliduj()
        {
    
             return true;

        }

        public override string ToString()
        {
            return GetArea() + " " + GetCicrum() + sides[0] + sides[1];
        }
    }
}
