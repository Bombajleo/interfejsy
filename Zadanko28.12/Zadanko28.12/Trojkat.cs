﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanko28._12
{
    class Trojkat : IShape
    {
        private float Area;
        private float Circum;

        private List<int> sides;
        public Trojkat()
        {
            RandomizeShape();
        }
        public float GetArea()
        {
            return Area;
        }

        public float GetCicrum()
        {
            return Circum;
        }

        public IShape RandomizeShape()
        {
            sides = new List<int>();
            Random rand = new Random();

            do
            {
                for (int i = 0; i < 3; i++)
                {
                    sides.Add(rand.Next(1, 100));
                }
            } while (Waliduj());
            return this;
        }
        public IShape RandomizeShape(Random rand)
        {
            sides = new List<int>();


            do
            {
                for (int i = 0; i < 3; i++)
                {
                    sides.Add(rand.Next(1, 100));
                }
            } while (Waliduj());
            return this;
        }
        public float SetArea()
        {
            sides.Sort();

            int a = sides[0];
            int b = sides[1];
            int c = sides[2];

            Area = (float)Math.Sqrt((a + b + c) * (a + b - c) * (a - b + c) * (-a + b + c)) / 4f;
            return Area;
        }

        public float SetCircum()
        {
            return Circum = sides.Sum();
        }

        public bool Waliduj()
        {
            int a = sides[0];
            int b = sides[1];
            int c = sides[2];

            if (a + b > c && a + c > b && b + c > a)
                return true;

            return false;
        }
        public override string ToString()
        {
            return GetArea() + " " + GetCicrum() + sides[0] + sides[1] + sides[2];
        }
    }
}