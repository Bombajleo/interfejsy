﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanko28._12
{
    interface IShape
    {
        float GetArea();
        float GetCicrum();
        IShape RandomizeShape(Random rand);
        IShape RandomizeShape();

        float SetArea();
        float SetCircum();
        bool Waliduj();
    }
}
